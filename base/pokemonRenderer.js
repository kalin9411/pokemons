export class PokemonRenderer {

    _pokemonDB
    _battleManager

    get battleManager() {return this._battleManager}
    get pokemonDB() {return this._pokemonDB}
    


    constructor(pokemonDB, battleManager) {this._pokemonDB = pokemonDB; this._battleManager = battleManager}


    playBackgroundMusic() {
    
        function sound(src) {
            this.sound = document.createElement("audio");
        
            this.sound.src = src;
            this.sound.setAttribute("preload", "auto");
            this.sound.setAttribute("controls", "none");
            this.sound.style.display = "none";
            this.sound.volume = 0.2
            document.body.appendChild(this.sound);
            this.play = function(){
              this.sound.play();
            }
            this.stop = function(){
              this.sound.pause();
            }
          }
      }


    refreshSelectedPokemonsWithDB(clickedid, enemyid) {
        console.log('referesh pokemons in battlemanager')
        this.battleManager._enemyPokemon = JSON.parse(JSON.stringify(this.pokemonDB.findPokemonById(enemyid)))
        this.battleManager._clickedPokemon = JSON.parse(JSON.stringify(this.pokemonDB.findPokemonById(clickedid)))
    }

    animateClickedPokemonAttack(message) {
        function sound(src) {
            this.sound = document.createElement("audio");
        
            this.sound.src = src;
            this.sound.setAttribute("preload", "auto");
            this.sound.setAttribute("controls", "none");
            this.sound.style.display = "none";
            document.body.appendChild(this.sound);
            this.play = function(){
              this.sound.play();
            }
            this.stop = function(){
              this.sound.pause();
            }
          }
        let hit = new sound('./assets/hit.wav');
        const battlecanvas = document.getElementById(`battlecanvas`)
        const clickedimg = document.getElementById(`clickedPokemon`)
        const enemyimg = document.getElementById(`enemyPokemon`)
        const ctx = battlecanvas.getContext("2d")
        let clickedX = 50
        let clickedY = 120
        const animate = () => {

            if (clickedX<=260) {
            requestAnimationFrame(animate)
            
            console.log('animating')

            ctx.clearRect(clickedX, clickedY, 96,96)
            // ctx.fillRect(clickedX,clickedY, 96, 96)
            ctx.drawImage(clickedimg, clickedX, clickedY)
            clickedX+=2
            clickedY-=0.5
            } else {
                setTimeout(() => {ctx.clearRect(350, 30, 90, 90);hit.play()}, 500)
                setTimeout(() => {ctx.drawImage(enemyimg, 350, 30)}, 600)
                setTimeout(() => {ctx.clearRect(350, 30, 90, 90)}, 700)
                setTimeout(() => {ctx.drawImage(enemyimg, 350, 30)}, 800)
                setTimeout(() => {ctx.clearRect(350, 30, 90, 90)}, 900)
                setTimeout(() => {ctx.drawImage(enemyimg, 350, 30)}, 1000)
                setTimeout(() => {
                    const returnAnimation = () => {
                
                        console.log('animating return')
                        
                        ctx.clearRect(50,120, 96, 96)
                        if (clickedX >= 50) {
                            requestAnimationFrame(returnAnimation)
                            ctx.clearRect(clickedX, clickedY, 96,96)
                            // ctx.fillRect(clickedX,clickedY, 96, 96)
                            ctx.drawImage(clickedimg, clickedX, clickedY)
                            clickedX-=2
                            clickedY+=0.5
                        } else {
                            ctx.drawImage(clickedimg, 50, 120)
                            console.log('animation finished.')
                            this.renderBattleScreen(message)
                            this.renderIdlePokemons()
                            this.renderHpBars()
                            return {}
                            
                        }
                    }
                    //re-render with the outcome before return animation
                    this.renderHpBars()
                    returnAnimation()
                
                }, 1500)
            }  
        }
        animate()
    }

    animateEnemyPokemonAttack(message) {
        function sound(src) {
            this.sound = document.createElement("audio");
        
            this.sound.src = src;
            this.sound.setAttribute("preload", "auto");
            this.sound.setAttribute("controls", "none");
            this.sound.style.display = "none";
            document.body.appendChild(this.sound);
            this.play = function(){
              this.sound.play();
            }
            this.stop = function(){
              this.sound.pause();
            }
          }
        let hit = new sound('./assets/hit.wav');
        const battlecanvas = document.getElementById(`battlecanvas`)
        const clickedimg = document.getElementById(`clickedPokemon`)
        const enemyimg = document.getElementById(`enemyPokemon`)
        const ctx = battlecanvas.getContext("2d")
            
        let enemyX = 350
        let enemyY = 30

        const animate = () => {

            if (enemyX >=130) {
            requestAnimationFrame(animate)
            
            console.log('animating')
            ctx.clearRect(enemyX, enemyY, 96,96)
            // ctx.fillRect(enemyX,clickedY, 96, 96)
            ctx.drawImage(enemyimg, enemyX, enemyY)
            enemyX-=2
            enemyY+=0.5
            } else {
                setTimeout(() => {ctx.clearRect(50, 120, 90, 90);hit.play()}, 500)
                setTimeout(() => {ctx.drawImage(clickedimg, 50, 120)}, 600)
                setTimeout(() => {ctx.clearRect(50, 120, 90, 90)}, 700)
                setTimeout(() => {ctx.drawImage(clickedimg, 50, 120)}, 800)
                setTimeout(() => {ctx.clearRect(50, 120, 90, 90)}, 900)
                setTimeout(() => {ctx.drawImage(clickedimg, 50, 120)}, 1000)
                setTimeout(() => {
                    let returnAnimation = () => {

                        console.log('animating return')
                        
                        ctx.clearRect(350,30, 96, 96)
                        if (enemyX <= 350) {
                            requestAnimationFrame(returnAnimation)
                            ctx.clearRect(enemyX, enemyY, 96,96)
                            // ctx.fillRect(enemyX,clickedY, 96, 96)
                            ctx.drawImage(enemyimg, enemyX, enemyY)
                            enemyX+=2
                            enemyY-=0.5
                        } else {
                            ctx.drawImage(enemyimg, 350, 30)
                            console.log('animation finished.')
                            this.renderBattleScreen(message)
                            this.renderIdlePokemons()
                            this.renderHpBars()
                            return {}
                        }
                    }
                    //re-render with the outcome before return animation
                    returnAnimation()
                    this.renderHpBars()

            }, 1500)
            }  
        }
        animate()
    } 


    renderIndividualPokemon(pokemon) {
        // console.log(pokemon)
        let createdcanvas = document.createElement("canvas")
        createdcanvas.setAttribute("id", `canvas${pokemon.id}`)
        createdcanvas.width = 250
        createdcanvas.height = 500
        document.body.append(createdcanvas)
        let canvas = document.getElementById(`canvas${pokemon.id}`)

        //click event
        canvas.addEventListener("click", () => {
            let randomNumber = Math.floor(Math.random()*20)
            this.refreshSelectedPokemonsWithDB(pokemon.id, randomNumber)
    
            this.renderBattleScreen('click to progress battle')
            this.renderHpBars()
            this.renderIdlePokemons()
        })
        ////////////////

        //img draw
        let ctx = canvas.getContext("2d")
        let img = document.createElement("img")
        img.src = pokemon.sprites.front_default;
        img.onload = function() {ctx.drawImage(img, 80, 0)}
        canvas.appendChild(img)
        ////////////////
        ctx.fillStyle = "black";
        ctx.font = "15px Courier New"
        ctx.fillText(`Name: ${pokemon.name}`, 50, 100)
        ctx.fillText(`Ability: ${pokemon.name}`, 50, 130)
        ctx.fillText(`HP: ${pokemon.stats[0].base_stat}`, 50, 160)
        ctx.fillText(`Move1: ${pokemon.moves[0].move.name}`, 50, 190)
        ctx.fillText(`Move2: ${pokemon.moves[1].move.name}`, 50, 220)
        ctx.fillText(`Move3: ${pokemon.moves[2].move.name}`, 50, 250)
        ctx.fillText(`Move4: ${pokemon.moves[3].move.name}`, 50, 280)
        ctx.fillText(`Speed: ${pokemon.stats[5].base_stat}`, 50, 310)
        ctx.fillText(`Special Defense: ${pokemon.stats[4].base_stat}`, 50, 340)
        ctx.fillText(`Special Attack: ${pokemon.stats[3].base_stat}`, 50, 370)
        ctx.fillText(`Defense: ${pokemon.stats[2].base_stat}`, 50, 400)
        ctx.fillText(`Attack: ${pokemon.stats[1].base_stat}`, 50, 430)
    }

    renderAllPokemons() {

        this.pokemonDB.pokemons.forEach((element) => {
            this.renderIndividualPokemon(element)
        })
    }

    renderHpBars() {
        let clickedPokemonDB = this.pokemonDB.findPokemonById(this.battleManager._clickedPokemon.id)
        let enemyPokemonDB = this.pokemonDB.findPokemonById(this.battleManager._enemyPokemon.id)
        let canvas = document.getElementById(`battlecanvas`)
        let ctx = canvas.getContext("2d")
        // text & hpbars
        ctx.beginPath();
        ctx.fillStyle = "white"
        //clear
        ctx.clearRect(390, 130, 200, 80)
        ctx.clearRect(40, 20, 180,80)
        ctx.lineWidth = "1";
        ctx.rect(400, 170, 100, 10);
        ctx.stroke();
        ctx.font = '15px Courier New';
        ctx.fillText(`${this.battleManager._clickedPokemon.name} ${this.battleManager._clickedPokemon.stats[0].base_stat} / ${clickedPokemonDB.stats[0].base_stat}`, 50, 50)
        
        ctx.rect(50, 70, 100, 10);
        ctx.stroke();
        ctx.font = '15px Courier New';
        ctx.fillText(`${this.battleManager._enemyPokemon.name} ${this.battleManager._enemyPokemon.stats[0].base_stat} / ${enemyPokemonDB.stats[0].base_stat}`, 400, 150)

        ctx.fillStyle = 'limegreen'
        ctx.fillRect(400, 170, this.battleManager._enemyPokemon.stats[0].base_stat/enemyPokemonDB.stats[0].base_stat*100, 10)
        ctx.fillRect(50, 70, this.battleManager._clickedPokemon.stats[0].base_stat/clickedPokemonDB.stats[0].base_stat*100, 10)

    }

    renderEndScreen(string) {
        let button = document.getElementById('restartbutton')
        let battlecanvas = document.getElementById(`battlecanvas`)
        const clickedimg = document.getElementById(`clickedPokemon`)
        const enemyimg = document.getElementById(`enemyPokemon`)
        clickedimg.remove()
        enemyimg.remove()
        let ctx = battlecanvas.getContext("2d")
        ctx.font = '35px Courier New'
        ctx.fillText(string, 200, 150)
        button.style.visibility = 'visible'
        let eventlistenerRestartFunction = () => {
            // button.removeEventListener('click', eventlistenerRestartFunction)
            // button.style.visibility = 'hidden'
            // this.refreshSelectedPokemonsWithDB(this._battleManager._clickedPokemon.id, this._battleManager._enemyPokemon.id)
            // this.renderBattleScreen()
            // this.renderHpBars()
            // this.renderIdlePokemons()
            this.closeBattleScreen()
            button.style.visibility = 'hidden'
        }
        button.addEventListener('click', eventlistenerRestartFunction)
    }

    closeBattleScreen() {
        this.battleManager._turnNumber = 0
        const battlecanvas = document.getElementById(`battlecanvas`)
        const container = document.getElementById(`container`)
        const clickedimg = document.getElementById(`clickedPokemon`)
        const enemyimg = document.getElementById(`enemyPokemon`)
        clickedimg.remove()
        enemyimg.remove()
        // battlecanvas.removeChild()
        battlecanvas.style.visibility = 'hidden'
        container.style.visibility = 'hidden'

    
    }

    renderIdlePokemons() {

        let battlecanvas = document.getElementById(`battlecanvas`)
        let ctx = battlecanvas.getContext("2d")
        let clickedimg = document.createElement("img")
        clickedimg.src = this.battleManager._clickedPokemon.sprites.back_default;
        clickedimg.id = `clickedPokemon`
        clickedimg.onload = function() {ctx.drawImage(clickedimg, 50, 120)}
        battlecanvas.append(clickedimg)
        
        let enemyimg = document.createElement("img")
        enemyimg.src = this.battleManager._enemyPokemon.sprites.front_default;
        enemyimg.id = `enemyPokemon`
        enemyimg.onload = function() {ctx.drawImage(enemyimg, 350, 30)}
        battlecanvas.append(enemyimg)

    }

    renderBattleScreen(message) {
 
        let battlecanvas = document.getElementById(`battlecanvas`)
        let ctx = battlecanvas.getContext("2d")
        ctx.clearRect(0, 0, battlecanvas.width, battlecanvas.height)
        battlecanvas.width = 600
        battlecanvas.height = 250
        ctx.fillStyle = "white"
        ctx.font = '15px Courier New';
        if (message == undefined) {message = 'click to progress battle'}
        ctx.fillText(message, 160, 240)

        // detect end of battle
        if (this.battleManager._clickedPokemon.stats[0].base_stat <= 0 ) {
           this.renderEndScreen('You lose.')
            return {}
        }
        if (this.battleManager._enemyPokemon.stats[0].base_stat <= 0 ) {
            this.renderEndScreen('You win!')
            return {}
        }

        // event listener logic (one-click only)
        const eventListenerFunction = () => {
            battlecanvas.removeEventListener("click", eventListenerFunction)
            console.log('detected click')
            const battleresult = this.battleManager.progressBattle(); 

            if (battleresult.damage === 0) {
                this.renderBattleScreen(battleresult.message)
                this.renderHpBars()
                this.renderIdlePokemons()
                // battlecanvas.addEventListener("click", eventListenerFunction)
            } else {
                if (this.battleManager._clickedIsLastAttacker)
                
                 {this.animateClickedPokemonAttack(battleresult.message)
                } else {this.animateEnemyPokemonAttack(battleresult.message)}
            }
        }
        
        battlecanvas.style.visibility = 'visible'
        console.log('..resuming event listener')
        battlecanvas.addEventListener("click", eventListenerFunction)
        this.playBackgroundMusic()

    }

}