export class PokemonDB {

    _pokemons = []
get pokemons() {return this._pokemons}

findPokemonById(id) {
    return this._pokemons.find((pokemon) => pokemon.id === id)

}

async fetchList() {
    return (await (await fetch('https://pokeapi.co/api/v2/pokemon/')).json()).results;
}

async fetchPoks(pok) {
    return await (await fetch(pok.url)).json()
}

async load() {
    const list = await this.fetchList()
    // console.log(list)
    await Promise.all(list.map(async url => {
        const pok = await this.fetchPoks(url);
        // console.log(pok);
        this._pokemons.push(pok)
    }))
}
}