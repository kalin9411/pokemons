export class BattleManager {

    _clickedPokemon
    _enemyPokemon
    _turnNumber
    _clickedIsLastAttacker 
    _damageModifier

    get damageModifier() {return this._damageModifier}
    set damageModifier(damageModifier) {this._damageModifier = damageModifier}
    // get clickedPokemon() {return this._clickedPokemon}
    // get enemyPokemon() {return this._enemyPokemon}
    constructor() {this._turnNumber = 0}


    damageToEnemyPokemon(damage) {
        if (damage >= this._enemyPokemon.stats[0].base_stat) {
            this._enemyPokemon.stats[0].base_stat = 0
        } else {
        this._enemyPokemon.stats[0].base_stat-= damage
        }
        this._clickedIsLastAttacker = true
    }

    damageToClickedPokemon(damage) {
        if (damage >= this._clickedPokemon.stats[0].base_stat) {
            this._clickedPokemon.stats[0].base_stat = 0
        } else {
        this._clickedPokemon.stats[0].base_stat-=damage
        }
        this._clickedIsLastAttacker = false

    }

    progressBattle() {
    
        let message 
        let damage
        // detecting first turn 
        if (this._turnNumber === 0) {
  
            console.log('first turn')
            if (this._clickedPokemon.stats[5].base_stat > this._enemyPokemon.stats[5].base_stat) {
                damage = this.attack(this._clickedPokemon, this._enemyPokemon)
                message = `${this._clickedPokemon.name} attacked first , damage: ${damage}.`
                this.damageToEnemyPokemon(damage)
            } else {
                damage = this.attack(this._enemyPokemon, this._clickedPokemon)
                message = `${this._enemyPokemon.name} attacked first, damage: ${damage}.`
                this.damageToClickedPokemon(damage)
            }
        // end of first turn ////////////
        } else {
           if (this._clickedIsLastAttacker) {
                damage = this.attack(this._enemyPokemon, this._clickedPokemon)
                message = `${this._enemyPokemon.name} attacked, damage: ${damage}`
                this.damageToClickedPokemon(damage)
               
           } else { 
                damage = this.attack(this._clickedPokemon, this._enemyPokemon)
                message = `${this._clickedPokemon.name} attacked, damage: ${damage}`
                this.damageToEnemyPokemon(damage)

           }
        }
        this._turnNumber++
        // console.log(this._clickedIsLastAttacker)
        // console.log(this._turnNumber)
        return {message, damage}
    }

    attack(attackerPokemon, attackedPokemon) {
        return this.calculateDamage(attackerPokemon, attackedPokemon)

    }
    calculateDamage(attackerPokemon, attackedPokemon) {
        const damage = Math.floor(attackerPokemon.stats[1].base_stat/attackedPokemon.stats[2].base_stat*Math.floor(Math.random()*this._damageModifier))
        // console.log(`damage ${damage}`)
        return damage
    }

}
