import {BattleManager} from './base/battleManger.js'
import {PokemonDB} from './base/pokemonDB.js'
import {PokemonRenderer} from './base/pokemonRenderer.js'

async function start() {
    const pokemondb = new PokemonDB()
    await pokemondb.load()
    const battleManager = new BattleManager()
    battleManager.damageModifier = 200
    const renderer = new PokemonRenderer(pokemondb, battleManager)
    console.log(pokemondb[0])
    renderer.renderAllPokemons()
}   
window.onload = start()